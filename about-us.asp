<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="header.inc"-->
		<main id="main">
			<section class="banner inner">
				<img src="images/img-banner-about.jpg" alt="image banner" style="margin-left: auto; margin-right: auto;">
				<div class="caption-holder">
					<div class="container">
						<div class="caption-block">
							<div class="caption">
								<h1><img src="images/icon-logo.png" alt="icon logo"> <span>Who We Are</span></h1>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="about-holder">
				<div class="container">
					<h2>About Us</h2>
					<img src="images/esd.png" alt="image" style="display:none;">
					<div class="two-columns">
						<div class="column">
							<p>In modern business, organizations must use technology to keep up with the competition and drive their success, no matter 
								what the organization does or whom they sell to. Formulating the right mix of hardware, software and systems can promote 
								productivity, improve customer satisfaction and even shorten a delivery life cycle. Most importantly, organizations that 
								successfully leverage technology will see the benefits where they matter most: on their bottom line. If you’re looking for 
								a trusted partner to provide optimized solutions for your organization, you’ve come to the right place.</p>
						</div>
						<div class="column">
							<p>Our team has broad development experience in using Agile methodologies to build applications in industry leading frameworks 
								and languages including:  HTML, CSS, JavaScript, C#, Java, Angular, ASP.NET, and Responsive Design. We also have significant 
								mobility design experience and have expertise in mobile applications both on Android and iOS platforms.
							</p>
						</div>
					</div>
					<div class="two-columns">
						<div class="column">
                            <p>SaQus is a <a href="images/MWBE Certification.pdf" target="_blank">New York State</a> and 
                                <a href="https://www.sdo.osd.state.ma.us/BusinessDirectory/BusinessDirectoryList.aspx?BusinessTypeID=0&CertTypeMBE=False&CertTypeWBE=True&CertTypePBE=False&CertTypeDBE=False&CertTypeACDBE=False&IndustryType=0&Keywords=saqus&ProductID=0&ProductNAICS=0&ProductNAICSCode=&SortedBy=Business_Name&SortedByName=Company+Name&SearchType=SOMWBA,TPCB&CityOrTown=&RegionNumber=0&ZipCode=&CertTypeTPCB=True&CertTypeVBE=False" target="_blank">Commonwealth of Massachusetts</a>
                                certified woman (WBE) owned custom software development and consulting firm, with its headquarters located in the Capital District
                                of New York, and an office in Falmouth, Massachusetts. We are dedicated to helping private and public sector organizations achieve
                                their long-term goals through the right application and management of information technology. We work with organizations and teams
                                that are looking for productivity software in order to build, maintain and optimize their operations – all without breaking the
                                bank.</p>
						</div>
						<div class="column">
							<p>SaQus is dedicated to bringing clients technology solutions that help decrease stress, streamline work and make your investment a profitable one. 
								SaQus brings years of experience and knowledge to our customers and helps them make their organizations more successful through better technology solutions.</p>
						</div>
					</div>
				</div>
			</section>
			<section class="team-block">
				<div class="container">
					<img src="images/img-about.png" alt="image top" class="top-img">
					<h2>Our Team</h2>
					<div class="team">
						<div class="img-block"><img src="images/img-joelle01.jpg" alt="image joelle"></div>
						<div class="text-block">
							<div class="heading-section" id="B1">
								<div class="left-col">
									<h3>Joelle Carmichael</h3>
									<span class="info">Managing Member</span>
								</div>
								<ul class="social">
									<li style="display:none;"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li style="display:none;"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="https://www.linkedin.com/in/joelle-carmichael-6b12373/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
							</div>
							<p>Joelle Carmichael is a successful business woman with more than 12 years overseeing the establishment and growth of SaQus. Joelle is responsible for all business
								 operations, business development, corporate strategy, recruiting, partnerships and business alliances. Her experience in leadership, human resource management, 
								 marketing, business development and organizational restructuring has led to SaQus’s exponential growth achieved as a result of successfully helping organizations 
								 increase efficiency, market share and customer service through business applications as well as the development of several complex customized systems for government
								 agencies. Successful recent applications developed under Joelle’s leadership include the award winning Open Book New York (contracts) and the VendRep application 
								 developed for the Office of the New York State Comptroller.</p>
							<p>Joelle is a graduate of the University at Buffalo, where she received a Bachelor of Science in Business Management. She also holds a Master’s Degree in Healthcare 
								Administration from Utica College.</p>
						</div>
					</div>
					<div class="team">
						<div class="img-block"><img src="images/img-matthew01.jpg" alt="image matthew"></div>
						<div class="text-block">
							<div class="heading-section" id="B2">
								<div class="left-col">
									<h3>Matthew Carmichael</h3>
									<span class="info">CIO</span>
								</div>
								<ul class="social">
									<li style="display:none;"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li style="display:none;"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="https://www.linkedin.com/in/matthewvcarmichael/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
							</div>
							<p>Matthew Carmichael has authored over 300 custom applications, and specializes in creating custom applications for organizations that have unique processes that cannot
								 be fulfilled by off-the-shelf software. He has created business critical applications for Saint Gobain Performance Plastics, SI Group, Rensselaer Polytechnic 
								 Institute (RPI), New York State Education Department, and Office of the New York State Comptroller.</p>
							<p>Matt has over 20,000 hours of Agile project experience serving in various roles including: project team member, scrum master, project manager, and product owner; in 
								addition to providing Agile training to project teams and managers. Matt specializes in presenting software development techniques to those without technical backgrounds.
							    His “easy to understand” approach provides an effective software development overview to executive management.</p>
							<p>Matt lives in Altamont, New York with his wife and two dogs.</p>
						</div>
					</div>
					<div class="team">
						<div class="img-block"><img src="images/img-gerard.jpg" alt="image gerard"></div>
						<div class="text-block">
							<div class="heading-section" id="B3">
								<div class="left-col">
									<h3>Gerard Uffelman</h3>
									<span class="info">Senior Architect</span>
								</div>
								<ul class="social" style="display:none;">
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								</ul>
							</div>
							<p>Gerard Uffelman is SaQus’s Senior Java \ Solution Architect. He is the team technical lead for many business critical applications that SaQus builds and supports for the 
								Office of New York State Comptroller (OSC). Gerard leads several OSC applications including the Online VendRep System , the State Authority Contracts Submission System 
								(CSS) for New York State Public Authorities, and the Ecteronic Document Submission System (EDSS).  Gerard is responsible for the overall design, development, and implementation
								 of these critical applications.</p>
							<p>Gerard has successfully completed many mission critical assignments at the State Comptroller Office, and has been the technical team leader and architect for the NYS Comptroller’s
								 IBM WebSphere application Server Conversion and the eDocs for Contracts project that interfaces with the Grant Gateway application and Statewide Financial System (SFS). His 
								 responsibilities include continuous application support and upgrades, formulating test plans, improving application security standards, setting up development environments, 
								 designing and coding web-based applications, creating external interfaces, and designing application graphics. Additionally, Gerard provides instruction and mentors technical 
								 staff in software standards. He has created and designed numerous organizations’ websites and has written several applications for online programming resources. As a software 
								 Architect, Gerard uses Agile/Scrum methodology to deliver on-time working software products.</p>
							<p>Gerard is an alum of Rensselaer Polytechnic Institute in Troy, NY, where he majored in Computer Sciences.</p>
						</div>
					</div>
					<div class="team">
						<div class="img-block"><img src="images/img-melissa01.jpg" alt="image melissa"></div>
						<div class="text-block">
							<div class="heading-section" id="B4">
								<div class="left-col">
									<h3>Melissa Stark</h3>
									<span class="info">Business Analyst</span>
								</div>
								<ul class="social">
									<li style="display:none;"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li style="display:none;"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="https://www.linkedin.com/in/melissa-stark-79a503a1/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
							</div>
							<p>Holding a degree in Business Administration with concentrations in Management Information Systems and Computer Science, Melissa Stark has special expertise in spearheading larger 
								software projects. Melissa has extensive public and private sector experience gathering requirements and managing projects. Her strong technical skills enhance her communications 
								between stakeholders and technical teams.</p>
							<p>Additional SaQus responsibilities include requirements analysis, software architecture, development, testing, training curriculum development and product delivery for both public 
								and private sector clients.</p>
						</div>
					</div>
					<div class="team">
						<div class="img-block"><img src="images/img-mperet01.jpg" alt="image mperet"></div>
						<div class="text-block">
							<div class="heading-section" id="B4">
								<div class="left-col">
									<h3>Matthew Peret</h3>
									<span class="info">Full Stack Developer</span>
								</div>
								<ul class="social">
									<li style="display:none;"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li style="display:none;"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="https://www.linkedin.com/in/matt-peret-5807b79" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
							</div>
							<p>Matt Peret has over twelve years experience as a software engineer working with private sector organizations, 
                                focusing on highly performant networking protocol and distributed database implementations. A quick study, Matt 
                                jumps into new projects with ease. Matt has significant experience with the entire software development lifecycle, 
                                from requirements gathering and refinement, through design and implementation, to debugging and direct customer support.</p>
							<p>Matt is an alumni of Worcester Polytechnic Institute, where he earned a Bachelor of Science in Computer Science.</p>
						</div>
					</div>
					<div class="team">
                            <div class="img-block"><img src="images/img-logan01.png" alt="image logan"></div>
                            <div class="text-block">
                                <div class="heading-section" id="B4">
                                    <div class="left-col">
                                        <h3>Logan Haygood</h3>
                                        <span class="info">Project Coordinator</span>
                                    </div>
                                    <ul class="social">
                                        <li style="display:none;"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li style="display:none;"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="https://www.linkedin.com/in/logan-haygood-659634183/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                                <p>Logan is a graduate of Framingham State University where he majored in Business and Information Technology. 
                                    This field allows him to effectively trace the most successful implementations of technology to help companies 
                                    quantify and exceed objectives. During his time in school, he took on a position at his alma mater’s Capital 
                                    Planning department, building and training the school’s populace for the implementation of a new work order 
                                    system. This experience fostered the importance of effective communication, a value learned in conjunction with 
                                    the experience of being an analyst in a new tech rollout for a Fortune 500 company. His robust experiences serve 
                                    as a repertoire to provide refreshing insight for any company or project.  </p>
                            </div>
                        </div>
                    </div>
			</section>
		</main>
<!--#include file="footer.inc"-->