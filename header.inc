<%
dim curPageName
dim tmpArr
tmpArr = Split(Request.ServerVariables("SCRIPT_NAME"),"/")
curPageName = tmpArr(UBound(tmpArr))
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="data-width=device-width, initial-scale=1.0">
	<title>Saqus</title>
	<link type="text/css" rel="stylesheet" href="style.css">
	<link type="text/css" rel="stylesheet" href="css/font-awesome.css">
	<link rel="icon" href="images/favicon.png" type="image/jpg" sizes="32x32">
</head>
<body>
	<div id="wrapper">
		<header id="header">
			<div class="header-top">
				<div class="container">
					<div class="left-block">
						<span class="tel"><i class="fa fa-phone" aria-hidden="true"></i> Call Us: <a href="tel:5188615661">518.861.5661</a></span>
						<ul class="social-networks">
							<li><a href="https://www.facebook.com/saqusit" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="https://twitter.com/saqusit" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.linkedin.com/company/11215625/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<a href="contact-us.asp" class="consultation">Free Consultation</a>
				</div>
			</div>
			<div class="header-bottom">
				<div class="container">
					<div class="logo"><a href="index.asp"><img src="images/logo.png" width="232" height="84" alt="Saqus"></a></div>
					<div class="info-block">
						<nav id="nav" class="open-close"> <a href="#" class="opener"><span></span></a>
							<div class="drop">
								<div class="slide">
									<ul>
										<li<% if curPageName = "index.asp" then Response.Write(" class='active'") end if %>><a href="index.asp">Home</a></li>
										<li<% if curPageName = "about-us.asp" then Response.Write(" class='active'") end if %>><a href="about-us.asp">About Us</a></li>
										<li<% if curPageName = "our-work.asp" then Response.Write(" class='active'") end if %>><a href="our-work.asp">Our Work</a></li>
										<li<% if curPageName = "contact-us.asp" then Response.Write(" class='active'") end if %>><a href="contact-us.asp">Contact Us</a></li>
									</ul>
								</div>
							</div>
						</nav>
						<div class="search-form" style="display:none;">
							<a class="form-opener" href="#"><i class="fa fa-search"></i></a>
							<form action="#" class="form-slide">
								<input type="search" class="search" placeholder="Search">
								<input type="submit" value="search">
							</form>
						</div>
					</div>
				</div>
			</div>
		</header>
