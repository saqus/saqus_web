<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="header.inc"-->
		<main id="main">
			<section class="banner inner">
				<img src="images/img-banner-work.jpg" alt="image banner" style="margin-left: auto; margin-right: auto;">
				<div class="caption-holder">
					<div class="container">
						<div class="caption-block">
							<div class="caption">
								<h1><img src="images/icon-logo.png" alt="icon logo"> <span>Our Work</span></h1>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="work-block">
				<div class="container">
					<h2>Work Portfolio</h2>
					<ul class="tabset" style="display:none;">
						<li class="active"><a href="#tab1">ALL</a></li>
						<li><a href="#tab2">APPLICATION DEVELOPMENT</a></li>
						<li><a href="#tab3">PUBLIC SECTOR</a></li>
						<li><a href="#tab4">PROJECT MANAGEMENT</a></li>
						<li><a href="#tab5">CLOUD HOSTING</a></li>
					</ul>
					<div class="tab-content">
						<ul class="accordion">
							<li>
								<a href="#" class="accordion-opener">All</a>
								<div id="tab1" class="accordion-slide">
									<div class="grid">
										<a href="#" class="grid-item">
											<img src="images/comptroller.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">Office of the New York State Comptroller</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry02.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">Rensselaer Polytechnic Institute (RPI)</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry03.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">Saint-Gobain Performance Plastics</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry04.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry05.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">Smith Dominelli and Guetti, LLC</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry06.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">KCS Land Research</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry07.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">National Corporate Benefits Administrators, Inc.</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry08.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">New York State Optometric Association (NYSOA)</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry09.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">Workforce Development Institute</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry10.jpg" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">New York State Municipalities</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry11.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">Capitol Payment Plan</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry12.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">New York State AFL-CIO</span>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
							</li>
							<li>
								<a href="#" class="accordion-opener">Application Development</a>
								<div id="tab2" class="accordion-slide">
									<div class="grid">
										<a href="#" class="grid-item">
											<img src="images/img-masonry04.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry06.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry01.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry02.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry03.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry05.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry07.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry08.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry09.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry10.jpg" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry11.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry12.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
							</li>
							<li>
								<a href="#" class="accordion-opener">Public Sector</a>
								<div id="tab3" class="accordion-slide">
									<div class="grid">
										<a href="#" class="grid-item">
											<img src="images/img-masonry08.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry10.jpg" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry01.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry02.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry03.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry04.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry05.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry06.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry07.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry09.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry11.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry12.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
							</li>
							<li>
								<a href="#" class="accordion-opener">Project Management</a>
								<div id="tab4" class="accordion-slide">
									<div class="grid">
										<a href="#" class="grid-item">
											<img src="images/img-masonry01.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry02.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry03.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry04.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry05.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry06.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry07.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry08.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry09.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry10.jpg" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry11.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry12.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
							</li>
							<li>
								<a href="#" class="accordion-opener">Cloud Hosting</a>
								<div id="tab5" class="accordion-slide">
									<div class="grid">
										<a href="#" class="grid-item">
											<img src="images/img-masonry04.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry06.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry01.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry02.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry03.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry05.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry07.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry08.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry09.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item">
											<img src="images/img-masonry10.jpg" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry11.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
										<a href="#" class="grid-item grid-item-width2">
											<img src="images/img-masonry12.png" alt="image">
											<span class="masonry-caption">
												<span class="caption-text">
													<span class="caption">
														<span class="content">
															<img src="images/icon-logo.png" alt="icon logo">
															<span class="text">SI Group - Manufacturer of Chemicals</span>
														</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</section>
		</main>
<!--#include file="footer.inc"-->