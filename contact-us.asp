<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="header.inc"-->
		<main id="main">
			<section class="banner inner">
				<img src="images/img-banner-contact.jpg" alt="image banner" style="margin-left: auto; margin-right: auto;">
				<div class="caption-holder">
					<div class="container">
						<div class="caption-block">
							<div class="caption">
								<h1><img src="images/icon-logo.png" alt="icon logo"> <span>Contact Us</span></h1>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="contact-block">
				<div class="container">
					<div class="two-blocks">
						<div class="block">
							<h2>Drop us a line. We'd love to chat</h2>
							<span class="info-text">Let us provide a custom solution for your organization’s needs.</span>
							<form action="saqus_email.asp" method="POST">
								<div class="fields-holder">
									<div class="fields">
										<div class="field name">
											<input type="text" name="emailtype" value="information" style="display:none;">
											<input type="text" placeholder="Your Name" required name="firstname">
										</div>
									</div>
									<div class="fields">
										<div class="field last">
											<input type="text" placeholder="Last Name" required name="lastname">
										</div>
									</div>
								</div>
								<div class="fields-holder">
									<div class="fields">
										<div class="field email">
											<input type="email" placeholder="Email Address" required name="email">
										</div>
									</div>
									<div class="fields">
										<div class="field company">
											<input type="text" placeholder="Company" required name="company">
										</div>
									</div>
								</div>
								<div class="field phone">
									<input type="tel" placeholder="Phone Number" required name="phone">
								</div>
								<div class="field textarea">
									<textarea cols="30" rows="10" placeholder="Message" required name="message"></textarea>
								</div>
								<input type="submit" value="Submit Now">
							</form>
						</div>
						<div class="block">
							<h2>Questions</h2>
							<ul class="accordion">
								<li>
									<a href="#" class="accordion-opener">Do you provide both large and small business solutions?</a>
									<div class="accordion-slide1">
										<p>Yes, we can develop a custom software product for any organization, from projects for large government agencies to solutions for small businesses.</p>
									</div>
								</li>
								<li>
									<a href="#" class="accordion-opener">What can a custom software solution do for you?</a>
									<div class="accordion-slide1">
										<p>A custom software solution can reduce stress, streamline work, and make your technology investment a profitable one.</p>
									</div>
								</li>
								<li>
									<a href="#" class="accordion-opener">I have many questions about custom software solutions. </a>
									<div class="accordion-slide1">
										<p>Great! Our team is happy to discuss your needs to help determine if a custom software solution is the right fit for your organization.</p>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<section class="map-holder" id="map-ny">
                <h2>New York Office Map</h2>
				<iframe src="https://maps.google.com/maps?q=300%20Great%20Oaks%20Blvd%20albany%2C%20ny&t=&z=13&ie=UTF8&iwloc=&output=embed" width="800" height="557" frameborder="0" style="border:0" allowfullscreen></iframe>
			</section>
			<section class="map-holder" id="map-ma">
                <h2><br />Massachusetts Office Map</h2>
                <iframe src="https://maps.google.com/maps?q=197%20palmer%20ave%2C%20falmouth%2C%20ma&t=&z=13&ie=UTF8&iwloc=&output=embed"" width="800" height="557" frameborder="0" style="border:0" allowfullscreen></iframe>
			</section>
		</main>
<!--#include file="footer.inc"-->