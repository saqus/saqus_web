<%  Dim EmailType
	EmailType = request.form("emailtype")
	
	If EmailType = "" Then
		Response.Redirect "contact-us.asp"
	End If
	If request.form("firstname") = "" Or request.form("lastname") = "" Or request.form("email") = "" Or request.form("company") = "" Then
		Response.Redirect "contact-us.asp"
	End If
	If request.form("phone") = "" Or request.form("message") = "" Then
		Response.Redirect "contact-us.asp"
	End If

	Dim MyURL
	MyURL = Request.ServerVariables("HTTP_REFERER") 

	Dim MyMail
	Set MyMail = CreateObject("CDO.Message")

	Session("InfoHdr") = "Contact Us Confirmation"
	Session("InfoMsg") = "Thank you for showing an interest in SaQus. We will contact you soon."

		'used to secure poast
		'If MyURL = "http://www.cappay.com/supply-orders.html"or MyURL = "http://cappay.com/supply-orders.html" Then
		
		If EmailType ="newsletter" Then
			
			MyMail.From = "NoReply@saqus.com"
			MyMail.To = "melissa@saqus.com, matt@saqus.com, mperet@saqus.com"
			MyMail.Subject = "Newsletter Subscriber"
			MyMail.TextBody ="Email: " &  request.form("email") & VbCrLf &_
							 "Message: " &  "I would like to subscribe to your newsletter." 
			MyMail.Send
			Set MyMail= nothing

			Session("InfoHdr") = "Newsletter Confirmation"
			Session("InfoMsg") = "Thank you! You are now subscribed to the SaQus newsletter."

			Response.Redirect "confirmation.asp"

		else
		
			MyMail.From = "NoReply@saqus.com"
			MyMail.To = "melissa@saqus.com, matt@saqus.com, mperet@saqus.com"
			MyMail.Subject = "SaQus Information Request from saqus.com"
			MyMail.TextBody ="Name: " & request.form("firstname") & " " & request.form("lastname") & VbCrLF &_
						 "Email: " &  request.form("email") & VbCrLf &_
						 "Company: " &  request.form("company") & VbCrLf &_
						 "Phone: " &  request.form("phone") & VbCrLf &_
						 "Message: " &  request.form("message") 
			MyMail.Send
			Set MyMail= nothing

			Response.Redirect "confirmation.asp"

		End If	
			
		'End If	
		
		'if Mailer.SendMail then
			'Response.Write "<table width=90% border=0><tr><td bgcolor=#ffff55 align=center>Mail sent</td></tr></table>"		
	'	else
		Response.Write ("Mail send failure, error has occurred")
		Response.Write ("<br><br>")
		Response.Write Request.ServerVariables("HTTP_REFERER")
	 '	end if

%>
	