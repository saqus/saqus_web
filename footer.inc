		<footer id="footer">
			<div class="footer-top">
				<div class="container">
					<div class="label-block">
						<label for="letter"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> <span>Subscribe To Newsletter</span></label>
					</div>
					<div class="form-block">
						<form action="saqus_email.asp" method="POST">
							<div class="field">
								<input type="text" name="emailtype" value="newsletter" style="display:none;">
								<input type="email" id="letter" required placeholder="Enter Email Address" name="email">
							</div>
							<input type="submit" value="Submit">
						</form>
					</div>
				</div>
			</div>
			<div class="footer-middle">
				<div class="container">
					<div class="column">
						<div class="logo"><a href="index.asp"><img src="images/logo01.png" width="210" height="78" alt="Saqus"></a></div>
						<p style="font-size:14px;">SaQus is a <a href="images/MWBE Certification.pdf" target="_blank">New York State</a>
                            and <a href="https://www.sdo.osd.state.ma.us/BusinessDirectory/BusinessDirectoryList.aspx?BusinessTypeID=0&CertTypeMBE=False&CertTypeWBE=True&CertTypePBE=False&CertTypeDBE=False&CertTypeACDBE=False&IndustryType=0&Keywords=saqus&ProductID=0&ProductNAICS=0&ProductNAICSCode=&SortedBy=Business_Name&SortedByName=Company+Name&SearchType=SOMWBA,TPCB&CityOrTown=&RegionNumber=0&ZipCode=&CertTypeTPCB=True&CertTypeVBE=False" target="_blank">Commonwealth of Massachusetts</a>
                            certified woman (WBE) owned
                            custom software development and consulting firm, with its headquarters located in the Capital District 
							of New York, and an office in Falmouth, Massachusetts. We are dedicated to helping private and public sector
                            organizations achieve their long-term goals through the right 
							application and management of information technology.</p>
						<a href="contact-us.asp" class="more">Contact Us</a>
					</div>
					<div class="column links">
						<span class="title">Quick Links</span>
						<ul class="menu">
							<li><a href="index.asp">Home</a></li>
							<li><a href="about-us.asp">About Us</a></li>
							<li><a href="our-work.asp">Our Work</a></li>
							<li><a href="contact-us.asp">Contact Us</a></li>
						</ul>
					</div>
					<div class="column contact">
						<span class="title">Contact Us</span>
						<span class="info">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
							<a href="contact-us.asp#map-ny">New York Office<br />300 Great Oaks Blvd<br>Suite 300<br>Albany, NY 12203</a>
						</span>
						<span class="info">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
							<a href="contact-us.asp#map-ma">Massachusetts Office<br />197 Palmer Ave<br>Falmouth, MA 02540</a>
						</span>
						<span class="info">
							<i class="fa fa-phone" aria-hidden="true"></i>
							<a href="tel:5188615661">518.861.5661</a>
						</span>
						<span class="info" style="display:none;">
							<i class="fa fa-envelope" aria-hidden="true"></i>
							<a href="mailto:info@saqusgmail.com">info@saqusgmail.com</a>
						</span>
					</div>
					<div class="column follow">
						<span class="title">Follow Us</span>
						<ul class="social-network">
							<li>
								<a href="https://www.facebook.com/saqusit" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> Like Us On Facebook</a>
							</li>
							<li>
								<a href="https://twitter.com/saqusit" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> Follow Us On Twitter</a>
							</li>
							<li>
								<a href="https://www.linkedin.com/company/11215625/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i> Share Us On Linkedin</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container">
					<p>Copyright © 2018-2019. <a href="index.asp">SaQus.com</a>. All Rights Reserved.</p> 
				</div>
			</div>
		</footer>
	</div>
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script src="http://straydogstudio.github.io/film_roll/js/vendor/jquery.touchSwipe.min.js"></script>
	<script src="js/jquery.film_roll.min.js"></script>
	<script type="text/javascript" src="js/jquery.main.js"></script>
	<script src="js/masonry.pkgd.min.js"></script>
</body>
</html>