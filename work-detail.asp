<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="header.inc"-->
		<main id="main">
			<section class="banner inner">
				<img src="images/img-banner-work.jpg" alt="image banner">
				<div class="caption-holder">
					<div class="container">
						<div class="caption-block">
							<div class="caption">
								<h1><img src="images/icon-logo.png" alt="icon logo"> <span>Our Work</span></h1>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="work-detail-block">
				<div class="container">
					<h2>SI Group - Manufacturer of Chemicals</h2>
					<div class="project-detail">
						<div class="img-block"><img src="images/img-project01.png" alt="image project"></div>
						<div class="text-block">
							<h3>Project: Oilfield Cloud Site</h3>
							<p>Designed and built custom website to provide dynamic information regarding SI Group Oilfields.</p>
							<ul>
								<li>Created context /model diagrams to document business logic and map to database / application objects</li>
								<li>Utilized .asp to create interface between cloud and database Design allowed for multi-language support</li>
								<li>Thoroughly analyzed local versus live sites and documented deltas</li>
								<li>Researched and proposed solutions</li>
							</ul>
							<div class="two-cols">
								<div class="col">
									<span class="title">Date:</span><span class="text">22 March 2017</span>
								</div>
								<div class="col">
									<span class="title">Category:</span><span class="text">Web Development</span>
								</div>
							</div>
						</div>
					</div>
					<div class="project-detail">
						<div class="img-block"><img src="images/img-project02.png" alt="image project"></div>
						<div class="text-block">
							<h3>Project: SRS Reporting Application</h3>
							<p>This Microsoft .Net cloud application allows worldwide users  to search for a variety of company reports using simple search and advanced search options.  Features include favorites, ratings and reviews, and report thumbnails/images. ASP.NET was used with a SQL Server database that housed local tables, views, linked servers, and stored procedures.</p>
							<p>The search tool provided an easy-to-use interface to a clunky off the shelf product.  While the off the shelf product allowed users to create a variety of reports, it did not organize them well or make it easy for non-technical users to view them.  This cloud solution allows users from various plant locations (other countries) to rank, share, and comment on reports allowing for better monitoring of daily operations.</p>
							<div class="two-cols">
								<div class="col">
									<span class="title">Date:</span><span class="text">22 March 2017</span>
								</div>
								<div class="col">
									<span class="title">Category:</span><span class="text">App Development</span>
								</div>
							</div>
						</div>
					</div>
					<div class="project-detail">
						<div class="img-block"><img src="images/img-project03.png" alt="image project"></div>
						<div class="text-block">
							<h3>Project: Interface to PeopleSoft SCM</h3>
							<p>Creation of custom interface to JD Edwards (now PeopleSoft SCM)</p>
							<ul>
								<li>Managed software team in development of a custom transportation interface to JD Edwards Supply Chain Management (now PeopleSoft)</li>
								<li>Worked on-site with chemical engineers to review current business process and develop business improvement strategy and model diagrams to track railcar and tank-wagon shipments</li>
								<li>Worked on-site with chemical engineers to review current business process and develop business improvement strategy and model diagrams to track railcar and tank-wagon shipments</li>
							</ul>
							<div class="two-cols">
								<div class="col">
									<span class="title">Date:</span><span class="text">22 March 2017</span>
								</div>
								<div class="col">
									<span class="title">Category:</span><span class="text">Software Development</span>
								</div>
							</div>
						</div>
					</div>
					<a href="our-work.asp" class="btn-back">Back to Our Work</a>
				</div>
			</section>
		</main>
<!--#include file="footer.inc"-->
