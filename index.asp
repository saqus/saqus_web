<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="header.inc"-->
		<main id="main">
			<section class="banner">
				<img src="images/img-banner.jpg" alt="image banner" style="margin-left: auto; margin-right: auto;">
				<div class="caption-holder">
					<div class="container">
						<div class="caption-block">
							<div class="caption">
								<h1>Building Custom Apps for Today's Business</h1>
								<ul>
									<li>Let us help your organization achieve its technology goals</li>
									<li>Our solutions improve customer satisfaction and revenue</li>
									<li>Increase productivity without breaking the bank!</li>
								</ul>
								<a href="about-us.asp">Learn More</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="services-block">
				<div class="container">
					<h2>Our Services</h2>
					<ul>
						<li>
							<div class="img-block">
								<span class="image">
									<span><img src="images/icon-app.png" alt="icon app"></span>
								</span>
							</div>
							<div class="text-block">
								<span class="title">Application Development</span>
								<p> Let us tailor a custom solution for your needs. Using Agile Methodologies, we will design a custom 
									software solution for your business that provides a level of flexibility unmatched by other firms.</p>
							</div>
						</li>
						<li>
							<div class="img-block">
								<span class="image">
									<span><img src="images/icon-sol.png" alt="icon sol"></span>
								</span>
							</div>
							<div class="text-block">
								<span class="title">Public Sector Solutions</span>
								<p>Extensive experience designing solutions for large scale projects including business critical 
									applications for a wide variety of private sector organizations including the NYS 
									Office of the Comptroller and the NYS Department of Education.</p>
							</div>
						</li>
						<li>
							<div class="img-block">
								<span class="image">
									<span><img src="images/icon-agile.png" alt="icon agile"></span>
								</span>
							</div>
							<div class="text-block">
								<span class="title">Agile Approach</span>
								<p>Our unique approach to software development, allows us to build products that are beneficial, practical, and budget friendly.</p>
							</div>
						</li>
						<li>
							<div class="img-block">
								<span class="image">
									<span><img src="images/icon-business.png" alt="icon business"></span>
								</span>
							</div>
							<div class="text-block">
								<span class="title">Business Analysis</span>
								<p>We will help address your needs with a thorough analysis of business process and tailor a solution 
									that will simplify your operations, save users time, and benefit the bottom line. </p>
							</div>
						</li>
						<li>
							<div class="img-block">
								<span class="image">
									<span><img src="images/icon-access.png" alt="icon access"></span>
								</span>
							</div>
							<div class="text-block">
								<span class="title">Microsoft Access Experts</span>
								<p>SaQus has delivered over 300 custom applications and our experienced development 
									team are experts in using Microsoft Access and VBA to design your business solutions. </p>
							</div>
						</li>
						<li>
							<div class="img-block">
								<span class="image">
									<span><img src="images/icon-archi.png" alt="icon archi"></span>
								</span>
							</div>
							<div class="text-block">
								<span class="title">Solution Architecting</span>
								<p>Modeling software and applications based on Business Users and Business Needs.</p>
							</div>
						</li>
						<li>
							<div class="img-block">
								<span class="image">
									<span><img src="images/icon-mnmgt.png" alt="icon management"></span>
								</span>
							</div>
							<div class="text-block">
								<span class="title">Project Management</span>
								<p>We will design a plan suited to your needs and follow it through with you from inception to final product.</p>
							</div>
						</li>
						<li>
							<div class="img-block">
								<span class="image">
									<span><img src="images/icon-cloud.png" alt="icon cloud"></span>
								</span>
							</div>
							<div class="text-block">
								<span class="title">Cloud Hosting</span>
								<p>Have the convenience of being able to access your data from anywhere (like the beach) - anytime</p>
							</div>
						</li>
					</ul>
				</div>
			</section>
			<section class="about-block">
					<h2>Our Work</h2>
					<ul class="tabset" style="display:none;">
						<li class="active"><a href="#tab1">ALL</a></li>
						<li><a href="#tab2">APPLICATION DEVELOPMENT</a></li>
						<li><a href="#tab3">PUBLIC SECTOR</a></li>
						<li><a href="#tab4">PROJECT MANAGEMENT</a></li>
						<li><a href="#tab5">CLOUD HOSTING</a></li>
					</ul>
					<div class="tab-content">
						<ul class="accordion">
							<li>
							<a href="#" class="accordion-opener">All</a>
							<div id="tab1" class="accordion-slide">
								<div id="film_roll_1">
									<div class="slide">
										<img src="images/img-slide.jpg" alt="image slide">
										<div class="info-text">
											<div class="column">
												<span class="title">PROJECT NAME</span>
												<span class="text">Rensselaer Polytechnic Institute (RPI)</span>
											</div>
											<div class="column">
												<span class="title">DATE</span>
												<span class="text">Since 2012</span>
											</div>
											<div class="column">
												<span class="title">WORK</span>
												<span class="text">Annual Budget &amp; Forecast Applications</span>
											</div>
											<div class="column">
												<span class="title">CATEGORY</span>
												<span class="text">Application Development</span>
											</div>
										</div>
									</div>
									<div class="slide">
										<img src="images/img-slide.jpg" alt="image slide">
										<div class="info-text">
											<div class="column">
												<span class="title">PROJECT NAME</span>
												<span class="text">Rensselaer Polytechnic Institute (RPI)</span>
											</div>
											<div class="column">
												<span class="title">DATE</span>
												<span class="text">Since 2012</span>
											</div>
											<div class="column">
												<span class="title">WORK</span>
												<span class="text">Annual Budget &amp; Forecast Applications</span>
											</div>
											<div class="column">
												<span class="title">CATEGORY</span>
												<span class="text">Application Development</span>
											</div>
										</div>
									</div>
									<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Polytechnic Institute (RPI)</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">Since 2012</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Annual Budget &amp; Forecast Applications</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
									</div>
									<a href="our-work.asp" class="more">View All</a>
								</div>
							</li>
							<li>
								<a href="#" class="accordion-opener">Application Development</a>
								<div id="tab2" class="accordion-slide">
									<div id="film_roll_2">
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
									</div>
									<a href="#" class="more">View All</a>
								</div>
							</li>
							<li>
								<a href="#" class="accordion-opener">Public Sector</a>
								<div id="tab3" class="accordion-slide">
									<div id="film_roll_3">
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
											</div>
										</div>
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
									</div>
									<a href="#" class="more">View All</a>
								</div>
							</li>
							<li>
								<a href="#" class="accordion-opener">Project Management</a>
								<div id="tab4" class="accordion-slide">
									<div id="film_roll_4">
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
									</div>
									<a href="#" class="more">View All</a>
								</div>
							</li>
							<li>
								<a href="#" class="accordion-opener">Cloud Hosting</a>
								<div id="tab5" class="accordion-slide">
									<div id="film_roll_5">
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
										<div class="slide">
											<img src="images/img-slide.jpg" alt="image slide">
											<div class="info-text">
												<div class="column">
													<span class="title">PROJECT NAME</span>
													<span class="text">Rensselaer Institute</span>
												</div>
												<div class="column">
													<span class="title">DATE</span>
													<span class="text">23/7/2017</span>
												</div>
												<div class="column">
													<span class="title">WORK</span>
													<span class="text">Web Design &amp; Develop</span>
												</div>
												<div class="column">
													<span class="title">CATEGORY</span>
													<span class="text">Application Development</span>
												</div>
											</div>
										</div>
									</div>
									<a href="#" class="more">View All</a>
								</div>
							</li>
						</ul>
					</div>
				</section>
			<section class="about-us-block">
				<div class="container">
					<h2>About Us</h2>
					<div class="img-holder"><img src="images/esd.png" alt="image about" style="display:none;"></div>
					<div class="four-columns">
						<div class="column">
							<div class="img-block">
								<img src="images/img-joelle.png" alt="image joelle">
							</div>
							<h3><a href="about-us.asp#B1">Joelle Carmichael</a></h3>
							<span class="info">Managing Member</span>
							<p>Joelle is a successful business woman with more than 13 years overseeing the establishment and growth of SaQus.</p>
							<a href="about-us.asp#B1" class="more">Learn More</a>
						</div>
						<div class="column">
							<div class="img-block">
								<img src="images/img-matthew.png" alt="image matthew">
							</div>
							<h3><a href="about-us.asp#B2">Matthew Carmichael</a></h3>
							<span class="info">CIO</span>
							<p>Matthew Carmichael specializes in presenting Agile\Lean software development processed to those without 
								technical backgrounds. His “easy to understand” approach provides an effective overview to executive management.</p>
							<a href="about-us.asp#B2" class="more">Learn More</a>
						</div>
						<div class="column">
							<div class="img-block">
								<img src="images/img-gerard.jpg" alt="image gerard">
							</div>
							<h3><a href="about-us.asp#B3">Gerard Uffelman</a></h3>
							<span class="info">Senior Architect</span>
							<p>Gerard Uffelman is our Senior Java \ Solution Architect. He is the team technical lead for many business
							   critical applications that SaQus builds and supports for the Office of New York State Comptroller.</p>
							<a href="about-us.asp#B3" class="more">Learn More</a>
						</div>
						<div class="column">
							<div class="img-block">
								<img src="images/img-melissa.png" alt="image melissa">
							</div>
							<h3><a href="about-us.asp#B4">Melissa Stark</a></h3>
							<span class="info">Business Analyst</span>
							<p>Melissa has extensive public and private sector experience gathering requirements and managing projects.   
							   Her strong technical skills enhance her communications between stakeholders and technical teams.</p>
							<a href="about-us.asp#B4" class="more">Learn More</a>
						</div>
					</div>
					<a href="about-us.asp" class="view-more">View All</a>
				</div>
			</section>
			<section class="product-block">
				<div class="container">
					<div class="holder">
						<h2>Our Featured Product</h2>
						<ul>
							<li>
								<div class="img-block"><img src="images/icon01.png" alt="icon"></div>
								<div class="text-block">
									<p>Our software helps solve the day to day headaches of unorganized teams by enabling them to work together in sync.</p>
								</div>
							</li>
							<li>
								<div class="img-block"><img src="images/icon02.png" alt="icon"></div>
								<div class="text-block">
									<p>Email notifications and user dashboards ensure that everyone on your team knows what needs to be done and when.</p>
								</div>
							</li>
							<li>
								<div class="img-block"><img src="images/icon03.png" alt="icon"></div>
								<div class="text-block">
									<p>Attachments utility allows users to attach files, images, and scan in documents. Keep your information organized and easily accessible.</p>
								</div>
							</li>
							<li>
								<div class="img-block"><img src="images/icon04.png" alt="icon"></div>
								<div class="text-block">
									<p>Dynamic reports provide critical information quickly to allow people to make better decisions.</p>
								</div>
							</li>
							<li>
								<div class="img-block"><img src="images/icon05.png" alt="icon"></div>
								<div class="text-block">
									<p>Client portal makes it easy for customers to interact with your organization.  Increase the speed of fulfillment.</p>
								</div>
							</li>
						</ul>
						<a href="contact-us.asp" class="link">Sign Up For A Free Consultation</a>
					</div>
				</div>
				<img src="images/img-product.png" alt="image products" class="img-products">
			</section>
		</main>
<!--#include file="footer.inc"-->