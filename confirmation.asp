<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="header.inc"-->
		<main id="main">
			<div style="text-align:center;">
				<h1><% response.Write(Session("InfoHdr")) %></h1>
				<h2>Your request has been sent.</h2>
				<p style="padding-top:50px; padding-bottom:50px;"><% response.Write(Session("InfoMsg")) %></p>
			</div>	
		</main>
<!--#include file="footer.inc"-->